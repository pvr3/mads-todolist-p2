
# Corrección de la práctica

### Documentación

- Has traducido las páginas del tutorial, pero eso era lo menos importante. No había que traducir, sino sólo explicar lo fundamental. Lo importante de la documentación era que explicaras la práctica, y no hay casi nada de eso.

### Código e implementación


- El controlador `Application.tasks` debería tomar un parámetro `Option[Int]`. El código:

        def tasks(orderParam: Boolean) = Action {
          if(orderParam){
            Application.order=true
            Ok(views.html.index(Task.all(Some(1))))
          }
          else{
            Application.order=false
            Ok(views.html.index(Task.all(None)))
          }
        }


    se haría mucho más sencillo y conciso:

            def tasks(orderParam: Option[Int]) = Action {
                order=orderParam
                Ok(views.html.index(Task.all(order)))
              }
            }

- Muy bien por controlar los errores y volver a pedir los formularios de creación/edición cuando el usuario introduce algún error.

- El *match* de `Task.all` debería contemplar todos los casos, para evitar un error cuando se le pase otro valor. En lugar de `None` se debería poner la opción por defecto (`case _`).

- Bien el `@if` en la vista con la lista de tareas

### Calificación

La implementación está muy bien, pero no has explicado la práctica en la documentación. Esto te baja un poco la nota.

Calificación: **0,3**  

21 de octubre de 2013
Domingo Gallardo

------

#Práctica 2 - Ampliando la aplicación mads-todolist
##Alumno: Pablo Verdú Romero  
------------------------------------------------------------

###1. Aplicación desplegada en Heroku.

La aplicación se encuentra desplegada en [este](http://mads-todolist.herokuapp.com) enlace de *Heroku*.

###2. Objetivos.

El objetivo principal de esta práctica es ampliar los conocimientos sobre el funcionamiento de *Play Framework* para Scala de la práctica anterior. En concreto, el objetivo principal es conocer el funcionamiento de los *Templates* y de los *Form* complejos. Para ello, se solicita ampliar la práctica anterior con las siguientes tareas:

1. Separar la pagina para crear tarea, añadiendo una nueva acción.

2. Refactorizar el programa para que los *Form* pasen de `Form[String]` a `Form[Task]`.

3. Añadir una nueva acción para editar una tarea.

4. Añadir una fecha limite a las tareas.

5. Cambiar la lista de tareas a una tabla.

6. Añadir la opcion de que las tareas se ordenen por la fecha limite o por el orden que fueron añadidas.

###3. Templates en Play Framework.

Los templates de Play Framework estan pensados para simplificar la tarea de representar los datos del modelo de dominio de tu aplicacion en una vista de tipo HTML, o ficheros de tipo XML o CSV, permitiendote empotrar codigo Scala dentro de la sintaxis de estos lenguajes.

Estos templates son compilados como si de clases Scala se trataran, por ejemplo, para una hipotetica vista `views/Application/index.scala.html` que recibe dos parametros llamados `customer` y `order`, se generaria una clase `views.html.Application.index` con un metodo `apply()`. Esto te permite que tu puedas llamar a dicha vista en cualquier parte de tu codigo Scala, siguiendo nuestro ejemplo, asi:

        val content = views.html.Application.index(c, o)

Para empotrar contenido dinamico en un template, utilizaremos el caracter `@` que indica a Play que la sentencia a continuacion se trata de codigo Scala. Por ejemplo:

        Hello @(customer.firstName + customer.lastName)!
       ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                    Codigo dinamico
                    
Para *escapar* un caracter `@` dentro de las sentencias Scala se utiliza la noticacion *doble arroba* `@@`.

Para indicar que un template recibe parametros, la primera linea de dicha plantilla debe indicar con el caracter *arroba* explicado anteriormente los parametros entre parentesis como si de una funcion Scala se tratase:

        @(customer: Customer, orders: List[Order])
        
Se sigue la misma convencion que en Scala para parametros por defecto y opcionales.

Tambien, se permite la inserccion de bucles para mostrar colecciones de datos de manera sencilla en una vista, utilizando esta notacion:

        <ul>
            @for(p <- products) {
              <li>@p.name ($@p.price)</li>
            }
        </ul>

Como se puede observar se coloca el caracter `@` delante de la sentencia form y llaves para delimitar el inicio y fin del bucle. Del mismo modo, para las sentencias if-else:

        @if(items.isEmpty) {
          <h1>Nothing to display</h1>
        } else {
          <h1>@items.size items!</h1>
        }

Tambien mediante el mismo caracter *arroba* es posible declarar funciones dentro de la vista y utilizarlas en otro lugar (Dentro de un bloque de etiquetas HTML, por ejemplo).

Por ultimo, se pueden definir valores reusables mediante la directiva `defining`:

        @defining(user.firstName + " " + user.lastName) { fullName =>
          <div>Hello @fullName</div>
        }

Es conveniente destacar que, para añadir comentarios se utiliza la notacion `@* *@` y que tambien se pueden hacer *imports* dentro del template.

###4. Forms en Play Framework.

Los *Forms* son una parte fundamental dentro de cualquier aplicacion web, Play incorpora herramientas para manejar forms simples de manera sencilla y posibilita el manejo de forms mas complejos.

Cuando se recibe una peticion POST, Play Framework busca los parametros de dicha peticion y los enlaza a un objeto de tipo `Form`. Dicho objeto, lo utiliza para rellenar una *Case Class* con sus datos pudiendo utilizar validaciones o restricciones...

Los forms, normalmente son utilizados desde instancias `Controller` y no tienen porque coindicir con *Case Class* o *modelos*, ademas, es razonable usar distintos `Form` para distintas peticiones POST.

Para usarlos hay que importar los siguientes paquetes:

        import play.api.data._
        import play.api.data.Forms._
        
####Definiendo un form.

Supongamos un ejemplo en el que tenemos la siguiente clase en el modelo:

    case class UserData(name: String, age: Int)
    
Para definir un `Form` que capture y enlace los datos a dicha clase se haria de la siguiente forma:

        val userForm = Form(
          mapping(
            "name" -> text,
            "age" -> number
          )(UserData.apply)(UserData.unapply)
        )
        
Como podemos observar el form crea un `Map` con los datos que necesitamos y sus restricciones. Ademas, tambien los metodos `apply` y `unapply` para la creacion y destruccion de la clase.

No estas limitado a usar *Case Class* en tu mapeo de forms, podrias utilizar tambien tuplas, pero es lo mas usual y lo que mas ventajas presenta.

####Definiendo restricciones en un form.

Como hemos visto en la creacion del `Map` del form, se añaden las restricciones de tipos que queremos que tengan esos valores, tambien podriamos definir rangos, valores por defecto... de la siguiente manera:

        val userFormConstraints2 = Form(
          mapping(
            "name" -> nonEmptyText,
            "age" -> number(min = 0, max = 100)
          )(UserData.apply)(UserData.unapply)
        )
        
En este ejemplo solicitamos que el campo `"name` debe ser texto y no puede estar vacio y que el campo `"age"` debe de estar comprendido en un rango de 0 a 100. En [este](http://www.playframework.com/documentation/2.2.x/api/scala/index.html#play.api.data.Forms$) enlace podemos obtener una lista de todas las restricciones definidas en el framework.

Tambien es posible la definicion de restricciones *ad-hoc* para usos especificos de tu aplicacion.

####Validacion de datos de un form.

Una vez tenemos definidas las restricciones para validar los datos de un form en una `Action` utilizamos el metodo `fold` el cual define dos funciones, una para cuando hay errores en la validacion y otra para cuando esta todo correcto:

        userForm.bindFromRequest.fold(
          formWithErrors => {
            // Error en la validacion.
            BadRequest(views.html.user(formWithErrors))
          },
          userData => {
            /* Validacion correcta */
            val newUser = models.User(userData.name, userData.age)
            val id = models.User.create(newUser)
            Redirect(routes.Application.home(id))
          }
        )

En este ejemplo, en el caso de que los datos sean erroneos se redirecciona de nuevo a la pagina del form con los errores como parametros. Si son correctos se crea una clase del modelo y se redirige a la pagina principal.

####Utilizando forms en un template.

Para habilitar un form dentro de una vista, necesitas pasarselo como parametro como hemos visto en los apartados anteriores. Despues para relacionar los campos de la vista con dicho form se hace de la siguiente manera:

        @helper.form(action = routes.Application.userPost()) {
          @helper.inputText(userForm("name"))
          @helper.inputText(userForm("age"))
        }

Como se puede observar, se define el metodo a ejecutar cuando se valide el form y los distintos campos asociados a cada parte del form. En nuestro caso, hay dos *inputs* para introducir los valores de nombre y edad.

Hay muchas herramientas proporcionadas por el framework para el manejo de forms en templates en el paquete [`views.html.helper`](http://www.playframework.com/documentation/2.2.x/api/scala/index.html#views.html.helper.package)

####Otras consideraciones importantes.

Tambien es posible el mapeo con tuplas, con simple, rellenar forms con unos datos concretos (Para la edicion de datos por ejemplo), anidar forms dentro de otros, utilizar listas para alguno de sus campos y valores opcionales o ignorados (Para cuadrarlos con una Clase del modelo). 







  
 





 

  