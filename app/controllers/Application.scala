package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import models.Task
import anorm._

object Application extends Controller {

  var order = false;

  val taskForm = Form(
    mapping(
      "id" -> ignored(NotAssigned:Pk[Long]),
      "label" -> nonEmptyText,
      "endDate" -> optional(date("yyyy-MM-dd"))
    )(Task.apply)(Task.unapply)
  )
  
  def index = Action {
    Redirect(routes.Application.tasks(Application.order))
  }

  def tasks(orderParam: Boolean) = Action {
    if(orderParam){
      Application.order=true
      Ok(views.html.index(Task.all(Some(1))))
    }
    else{
      Application.order=false
      Ok(views.html.index(Task.all(None)))
    }
  }

  def newTask = Action { implicit request =>
  	taskForm.bindFromRequest.fold(
      errors => BadRequest(views.html.newTask(errors)),
  		task => {
  			Task.create(task)
  			Redirect(routes.Application.tasks(Application.order))
  		}
  	)
  }

  //Action que borra la tarea pasada por parametro y te rederige a la lista de tareas.
  def deleteTask(id: Long) = Action {
    Task.delete(id)
    Redirect(routes.Application.tasks(Application.order))
  }

  //Action que responde enviandote a la pagina para crear una nueva tarea.
  def showNewTaskForm = Action {
    Ok(views.html.newTask(taskForm))
  }

  //Action que responde enviandote a la pagina para editar una nueva tarea.
  def editTask(id: Long) = Action {
    Task.findById(id).map { task =>
      Ok(views.html.editTask(id, taskForm.fill(task)))
    }.getOrElse(NotFound)
  }
  
  //Action que actualiza la informacion de una tarea.
  def updateTask(id: Long) = Action { implicit request =>
    taskForm.bindFromRequest.fold(
      errors => BadRequest(views.html.editTask(id, errors)),
      task => {
        Task.update(id, task)
        Redirect(routes.Application.tasks(Application.order))
      } 
    )
  }  
  
}